package com.gitlab.izanrodrigo.sngularcodingchallenge.di

import android.app.Application
import com.gitlab.izanrodrigo.sngularcodingchallenge.ui.uiModule
import org.koin.android.ext.android.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(appModule, uiModule))
    }
}