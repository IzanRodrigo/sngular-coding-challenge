package com.gitlab.izanrodrigo.sngularcodingchallenge.di

import com.gitlab.izanrodrigo.sngularcodingchallenge.network.NetworkClient
import com.gitlab.izanrodrigo.sngularcodingchallenge.util.SysInfo
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

val appModule = module {
    single {
        val lang = Locale.getDefault().country
        val (system, symbol) = when (lang.toUpperCase()) {
            "US", "LR", "MM" -> "imperial" to "F"
            else -> "metric" to "C"
        }
        SysInfo(lang, system, symbol)
    }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(NetworkClient.ENDPOINT)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(NetworkClient::class.java)
    }

    single {
        OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .addInterceptor {
                val sysInfo = get<SysInfo>()
                it.proceed(
                    it.request()
                        .newBuilder()
                        .url(
                            it.request()
                                .url()
                                .newBuilder()
                                .addQueryParameter("appid", NetworkClient.API_KEY)
                                .addQueryParameter("lang", sysInfo.lang)
                                .addQueryParameter("units", sysInfo.unitSystem)
                                .build()
                        )
                        .build()
                )
            }
            .build()
    }
}