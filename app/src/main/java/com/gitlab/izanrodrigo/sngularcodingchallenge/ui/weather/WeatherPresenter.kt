package com.gitlab.izanrodrigo.sngularcodingchallenge.ui.weather

import android.location.Location
import android.os.Parcelable
import androidx.annotation.DrawableRes
import com.gitlab.izanrodrigo.sngularcodingchallenge.R
import com.gitlab.izanrodrigo.sngularcodingchallenge.data.CurrentWeather
import com.gitlab.izanrodrigo.sngularcodingchallenge.network.NetworkClient
import com.gitlab.izanrodrigo.sngularcodingchallenge.util.TempFormatter
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.coroutines.experimental.bg
import java.text.DateFormat
import java.util.*

interface WeatherView {
    var isLoading: Boolean

    fun showWeather(weather: WeatherViewModel)
    fun showError(throwable: Throwable)
}

@Parcelize
data class WeatherViewModel(
    val cityId: Int,
    val cityName: String?,
    @DrawableRes val bgResID: Int,
    @DrawableRes val iconResID: Int,
    val description: String?,
    val date: String,
    val currentTemp: String,
    val minTemp: String,
    val maxTemp: String
) : Parcelable

class WeatherPresenter(
    private var view: WeatherView?,
    private val networkClient: NetworkClient,
    private val tempFormatter: TempFormatter
) {
    private var job: Job? = null

    fun loadWeather(location: Location) {
        job?.cancel()
        job = launch(UI) {
            view?.isLoading = true

            try {
                val weather = networkClient.getWeather(location.latitude, location.longitude).await()
                val vm = bg { weather.toViewModel(tempFormatter) }.await()
                view?.isLoading = false
                view?.showWeather(vm)
            } catch (ex: Throwable) {
                view?.isLoading = false
                view?.showError(ex)
            }
        }
    }

    fun onDestroy() {
        job?.cancel()
        job = null
        view = null
    }
}

fun CurrentWeather.toViewModel(tempFormatter: TempFormatter): WeatherViewModel {
    val isDay = when (sys.pod) {
        "d" -> true
        "n" -> false
        else -> when {
            sys.sunrise > 0 && sys.sunset > 0 -> dt >= sys.sunrise && dt <= sys.sunset
            else -> error("Failed to parse day from sys info: $sys")
        }
    }
    val bg = when {
        isDay -> R.drawable.day_bg
        else -> R.drawable.night_bg
    }
    val weather = weather.firstOrNull()
    val icon = when (weather?.icon) {
        "01d" -> R.drawable.ic_weather_clear
        "01n" -> R.drawable.ic_weather_clear_night
        "02d" -> R.drawable.ic_weather_few_clouds
        "02n" -> R.drawable.ic_weather_few_clouds_night
        "03d", "04d" -> R.drawable.ic_weather_clouds
        "03n", "04n" -> R.drawable.ic_weather_clouds_night
        "09d" -> R.drawable.ic_weather_showers_day
        "09n" -> R.drawable.ic_weather_showers_night
        "10d" -> R.drawable.ic_weather_rain_day
        "10n" -> R.drawable.ic_weather_rain_night
        "11d" -> R.drawable.ic_weather_storm_day
        "11n" -> R.drawable.ic_weather_storm_night
        "13d" -> R.drawable.ic_weather_snow_scattered_day
        "13n" -> R.drawable.ic_weather_snow_scattered_night
        "50d", "50n" -> R.drawable.ic_weather_mist
        else -> R.drawable.ic_weather_none_available
    }
    val date = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT)
        .format(Date(dt * 1000L))
    val current = tempFormatter.format(main.temp)
    val min = tempFormatter.format(main.temp_min)
    val max = tempFormatter.format(main.temp_max)

    return WeatherViewModel(
        id,
        name,
        bg,
        icon,
        weather?.description,
        date,
        current,
        min,
        max
    )
}