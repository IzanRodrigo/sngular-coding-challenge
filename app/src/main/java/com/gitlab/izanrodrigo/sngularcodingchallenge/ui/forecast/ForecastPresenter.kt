package com.gitlab.izanrodrigo.sngularcodingchallenge.ui.forecast

import android.os.Parcelable
import com.gitlab.izanrodrigo.sngularcodingchallenge.data.Forecast
import com.gitlab.izanrodrigo.sngularcodingchallenge.network.NetworkClient
import com.gitlab.izanrodrigo.sngularcodingchallenge.ui.weather.WeatherViewModel
import com.gitlab.izanrodrigo.sngularcodingchallenge.ui.weather.toViewModel
import com.gitlab.izanrodrigo.sngularcodingchallenge.util.TempFormatter
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.coroutines.experimental.bg
import java.util.concurrent.TimeUnit

interface ForecastView {
    var isLoading: Boolean

    fun showForecast(forecast: ForecastViewModel)
    fun showError(throwable: Throwable)
}

@Parcelize
data class ForecastViewModel(
    val cityId: Int,
    val cityName: String,
    val list: List<WeatherViewModel>
) : Parcelable

class ForecastPresenter(
    private var view: ForecastView?,
    private val cityId: Int,
    private val networkClient: NetworkClient,
    private val tempFormatter: TempFormatter
) {
    private var job: Job? = null

    fun loadForecast() {
        job?.cancel()
        job = launch(UI) {
            view?.isLoading = true
            delay(333, TimeUnit.MILLISECONDS) // This is just to show the refresh layout loading.

            try {
                val forecast = networkClient.getForecast(cityId).await()
                val vm = bg { forecast.toViewModel(tempFormatter) }.await()
                view?.isLoading = false
                view?.showForecast(vm)
            } catch (ex: Throwable) {
                view?.isLoading = false
                view?.showError(ex)
            }
        }
    }

    fun onDestroy() {
        job?.cancel()
        job = null
        view = null
    }
}

fun Forecast.toViewModel(tempFormatter: TempFormatter): ForecastViewModel {
    val list = this.list.map { it.toViewModel(tempFormatter) }
    return ForecastViewModel(city.id, city.name, list)
}