package com.gitlab.izanrodrigo.sngularcodingchallenge.ui

import com.gitlab.izanrodrigo.sngularcodingchallenge.ui.forecast.ForecastPresenter
import com.gitlab.izanrodrigo.sngularcodingchallenge.ui.forecast.ForecastView
import com.gitlab.izanrodrigo.sngularcodingchallenge.ui.weather.WeatherPresenter
import com.gitlab.izanrodrigo.sngularcodingchallenge.ui.weather.WeatherView
import com.gitlab.izanrodrigo.sngularcodingchallenge.util.TempFormatter
import org.koin.dsl.module.module

val uiModule = module {
    factory { TempFormatter(get()) }

    factory {
        val view = it.get<WeatherView>() as WeatherView
        WeatherPresenter(view, get(), get())
    }

    factory {
        val view = it.get<ForecastView>() as ForecastView
        val cityId = it.get<Int>() as Int
        ForecastPresenter(view, cityId, get(), get())
    }
}