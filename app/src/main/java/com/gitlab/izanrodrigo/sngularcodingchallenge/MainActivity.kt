package com.gitlab.izanrodrigo.sngularcodingchallenge

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private inline val navController
        get() = findNavController(R.id.nav_host_fragment)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        NavigationUI.setupWithNavController(toolbar, navController)
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    override fun onSupportNavigateUp() =
        navController.navigateUp()

    override fun setTitle(titleId: Int) {
        toolbarTitle.setText(titleId)
    }

    override fun setTitle(title: CharSequence?) {
        toolbarTitle.text = title
    }
}
