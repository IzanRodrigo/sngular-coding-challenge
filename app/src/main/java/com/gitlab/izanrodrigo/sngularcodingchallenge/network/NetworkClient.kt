package com.gitlab.izanrodrigo.sngularcodingchallenge.network

import com.gitlab.izanrodrigo.sngularcodingchallenge.data.CurrentWeather
import com.gitlab.izanrodrigo.sngularcodingchallenge.data.Forecast
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkClient {
    companion object {
        const val ENDPOINT = "https://api.openweathermap.org/data/2.5/"
        const val API_KEY = "6a7ff8c98d89f165a81b906b9d1f8f0a"
    }

    @GET("weather")
    fun getWeather(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double
    ): Deferred<CurrentWeather>

    @GET("forecast")
    fun getForecast(@Query("id") cityId: Int): Deferred<Forecast>
}