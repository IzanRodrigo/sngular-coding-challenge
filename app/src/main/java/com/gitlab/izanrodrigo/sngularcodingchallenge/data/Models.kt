package com.gitlab.izanrodrigo.sngularcodingchallenge.data

import com.squareup.moshi.Json

data class CurrentWeather(
    val clouds: Clouds,
    val cod: Int,
    val coord: Coord,
    val dt: Int,
    val id: Int,
    val main: Main,
    val name: String,
    val rain: Rain,
    val sys: Sys,
    val weather: List<Weather>,
    val wind: Wind
)

data class Forecast(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: List<CurrentWeather>,
    val message: Double
)

data class Coord(val lat: Double, val lon: Double)

data class Main(
    val humidity: Int,
    val pressure: Double,
    val temp: Double,
    val temp_max: Double,
    val temp_min: Double,
    val sea_level: Double,
    val grnd_level: Double
)

data class Weather(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)

data class Rain(@Json(name = "3h") val volume: Int)

data class Wind(val deg: Double, val speed: Double)

data class Clouds(val all: Int)

data class Sys(
    val pod: String?,
    val country: String,
    val sunrise: Int,
    val sunset: Int
)

data class City(
    val coord: Coord,
    val country: String,
    val id: Int,
    val name: String
)