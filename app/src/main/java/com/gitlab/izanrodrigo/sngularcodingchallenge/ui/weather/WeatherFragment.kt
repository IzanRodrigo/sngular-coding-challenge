package com.gitlab.izanrodrigo.sngularcodingchallenge.ui.weather

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.gitlab.izanrodrigo.sngularcodingchallenge.R
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.fragment_weather.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

private const val ARG_WEATHER_DATA = "ARG_WEATHER_DATA"
private const val REQUEST_CODE_LOCATION = 12345

class WeatherFragment : Fragment(), WeatherView {
    private val presenter: WeatherPresenter by inject { parametersOf(this) }
    private val locationClient by lazy { LocationServices.getFusedLocationProviderClient(requireActivity()) }
    private val settingsClient by lazy { LocationServices.getSettingsClient(requireActivity()) }

    private var data: WeatherViewModel? = null
    private var comingFromSettings = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        data = savedInstanceState?.getParcelable(ARG_WEATHER_DATA) as? WeatherViewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_weather, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refreshLayout.setColorSchemeResources(R.color.colorAccent)
        refreshLayout.setOnRefreshListener { checkLocation() }

        goToForecastButton.setOnClickListener {
            val cityId = data?.cityId ?: return@setOnClickListener
            val directions = WeatherFragmentDirections
                .actionForecastFragmentToForecastFragment2()
                .setCityId(cityId)
            findNavController().navigate(directions)
        }

        val data = data
        if (data == null) {
            checkPermissions()
        } else {
            showWeather(data)
            isLoading = false
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val activity = activity ?: return
        activity.setTitle(R.string.fragment_weather_label)
    }

    override fun onResume() {
        super.onResume()

        if (comingFromSettings) {
            comingFromSettings = false
            checkPermissions()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_LOCATION) {
            if (resultCode == Activity.RESULT_OK) {
                checkLocation()
            } else {
                showLocationDisabledError()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        data?.let { outState.putParcelable(ARG_WEATHER_DATA, it) }
    }

    override fun onDestroy() {
        data = null
        presenter.onDestroy()
        super.onDestroy()
    }

    // region WeatherView

    override var isLoading: Boolean
        get() = refreshLayout.isRefreshing
        set(value) {
            refreshLayout.isRefreshing = value
            content.visibility = if (data == null && value) View.GONE else View.VISIBLE
        }

    override fun showWeather(weather: WeatherViewModel) {
        data = weather
        infoGroup.visibility = View.VISIBLE
        content.setBackgroundResource(weather.bgResID)
        icon.setImageResource(weather.iconResID)
        subtitle.text = when {
            weather.description == null -> weather.cityName
            else -> getString(R.string.weather_subtitle_template, weather.cityName, weather.description)
        }
        currentTemp.text = weather.currentTemp
        minTemp.text = weather.minTemp
        maxTemp.text = weather.maxTemp
    }

    override fun showError(throwable: Throwable) {
        infoGroup.visibility = View.GONE
        subtitle.setText(R.string.load_weather_error)
        icon.setImageResource(R.drawable.ic_weather_none_available)
        icon.setOnClickListener { checkLocation() }
    }

    // endregion

    // region Internal

    private fun checkPermissions() {
        isLoading = true

        val activity = activity ?: return
        Dexter.withActivity(activity)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    if (response.isPermanentlyDenied) {
                        showPermissionsDeniedPermanentlyError()
                    } else {
                        showPermissionsDeniedError()
                    }
                }

                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    checkLocation()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            })
            .check()
    }

    @SuppressLint("MissingPermission") // Already handled.
    private fun checkLocation() {
        isLoading = true

        val locationRequest = LocationRequest.create()
        locationRequest.interval = 5000 // 5s
        locationRequest.fastestInterval = 2000 // 2s
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY // It's a one-time thing.

        val settingsRequest = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
            .build()
        settingsClient.checkLocationSettings(settingsRequest)
            .addOnSuccessListener {
                locationClient.lastLocation
                    .addOnSuccessListener { location ->
                        if (location != null) {
                            presenter.loadWeather(location)
                        } else { // Rare case, request location.
                            val callback = object : LocationCallback() {
                                override fun onLocationResult(result: LocationResult) {
                                    super.onLocationResult(result)
                                    locationClient.removeLocationUpdates(this)
                                    presenter.loadWeather(result.lastLocation)
                                }
                            }

                            locationClient.requestLocationUpdates(locationRequest, callback, Looper.myLooper())
                        }
                    }
            }
            .addOnFailureListener {
                if (it is ResolvableApiException) {
                    startIntentSenderForResult(it.resolution.intentSender, REQUEST_CODE_LOCATION,
                        null, 0, 0, 0, null)
                }
            }
    }

    private fun showLocationDisabledError() {
        isLoading = false

        // User doesn't enable the location service, show an snackbar with the error.
        Snackbar.make(view!!, R.string.error_location_disabled, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.retry_button) { checkLocation() }
            .show()
    }

    private fun showPermissionsDeniedError() {
        isLoading = false

        // User denies the app the location permission, show an snackbar with the error.
        Snackbar.make(view!!, R.string.error_permission_denied, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.retry_button) { checkPermissions() }
            .show()
    }

    private fun showPermissionsDeniedPermanentlyError() {
        isLoading = false

        // User permanently denies the app the location permission, show an snackbar with the error.
        Snackbar.make(view!!, R.string.error_permission_denied_permanently, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.settings_button) {
                // Add action to go to settings to enable the permissions.
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                    data = Uri.fromParts("package", requireActivity().packageName, null)
                    addCategory(Intent.CATEGORY_DEFAULT)
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                }
                startActivity(intent)
                comingFromSettings = true
            }
            .show()
    }

    // endregion
}