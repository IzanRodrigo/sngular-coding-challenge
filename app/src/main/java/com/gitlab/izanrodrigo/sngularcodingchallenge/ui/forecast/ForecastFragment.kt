package com.gitlab.izanrodrigo.sngularcodingchallenge.ui.forecast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gitlab.izanrodrigo.sngularcodingchallenge.R
import com.gitlab.izanrodrigo.sngularcodingchallenge.extensions.RecyclerAdapter
import com.gitlab.izanrodrigo.sngularcodingchallenge.extensions.RecyclerHolder
import com.gitlab.izanrodrigo.sngularcodingchallenge.ui.weather.WeatherViewModel
import kotlinx.android.synthetic.main.fragment_forecast.*
import kotlinx.android.synthetic.main.list_item_weather_info.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

private const val ARG_FORECAST_DATA = "ARG_FORECAST_DATA"

class ForecastFragment : Fragment(), ForecastView {
    private val presenter: ForecastPresenter by inject {
        val cityId = ForecastFragmentArgs.fromBundle(arguments).cityId
        parametersOf(this, cityId)
    }

    private var data: ForecastViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_forecast, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refreshLayout.setColorSchemeResources(R.color.colorAccent)
        refreshLayout.setOnRefreshListener { presenter.loadForecast() }

        recyclerView.layoutManager = LinearLayoutManager(context)

        retryButton.setOnClickListener { presenter.loadForecast() }

        val data = savedInstanceState?.getParcelable(ARG_FORECAST_DATA) as? ForecastViewModel
        if (data == null) {
            presenter.loadForecast()
        } else {
            showForecast(data)
            isLoading = false
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        updateTitle()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        data?.let { outState.putParcelable(ARG_FORECAST_DATA, it) }
    }

    override fun onDestroy() {
        data = null
        presenter.onDestroy()
        super.onDestroy()
    }

    // region WeatherView

    override var isLoading: Boolean
        get() = refreshLayout.isRefreshing
        set(value) {
            refreshLayout.visibility = View.VISIBLE
            refreshLayout.isRefreshing = value
            retryButton.visibility = View.GONE
            errorMessage.visibility = View.GONE
            recyclerView.visibility = if (data == null && value) View.GONE else View.VISIBLE
        }

    override fun showForecast(forecast: ForecastViewModel) {
        data = forecast
        updateTitle()
        recyclerView.adapter = WeatherAdapter(forecast.list)
    }

    override fun showError(throwable: Throwable) {
        refreshLayout.visibility = View.GONE
        retryButton.visibility = View.VISIBLE
        errorMessage.visibility = View.VISIBLE
    }

    // endregion

    // region Internal

    private fun updateTitle() {
        val cityName = data?.cityName
        activity?.title = when (cityName) {
            null -> getString(R.string.fragment_forecast_label)
            else -> getString(R.string.fragment_forecast_label_template, cityName)
        }
    }

    private class WeatherAdapter(
        private val list: List<WeatherViewModel>
    ) : RecyclerAdapter<RecyclerHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerHolder {
            return LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_weather_info, parent, false)
                .let(::RecyclerHolder)
        }

        override fun getItemCount() = list.size

        override fun onBindViewHolder(holder: RecyclerHolder, position: Int) {
            val item = list[position]

            holder.itemView.setBackgroundResource(item.bgResID)
            holder.icon.setImageResource(item.iconResID)
            holder.date.text = item.date
            holder.description.text = when {
                item.description == null -> item.currentTemp
                else -> "${item.currentTemp} - ${item.description}"
            }
            holder.temps.text = holder.itemView.context.getString(
                R.string.forecast_temps_template,
                item.minTemp, item.maxTemp
            )
        }
    }

    // endregion
}