package com.gitlab.izanrodrigo.sngularcodingchallenge.util

data class SysInfo(
    val lang: String,
    val unitSystem: String,
    val unitSymbol: String
)