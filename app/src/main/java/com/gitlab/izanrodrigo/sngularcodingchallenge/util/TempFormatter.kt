package com.gitlab.izanrodrigo.sngularcodingchallenge.util

import java.text.DecimalFormat

class TempFormatter(private val sysInfo: SysInfo) {
    private val format by lazy {
        DecimalFormat("#.#").apply { isDecimalSeparatorAlwaysShown = false }
    }

    fun format(temp: Double) = buildString {
        append(format.format(temp))
        append(" °")
        append(sysInfo.unitSymbol)
    }
}